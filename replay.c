#include "replay.h"
#include <alsa/asoundlib.h>
#include <alsa/control.h>

static snd_output_t *output = NULL;

int replay(const char *device) {
        int err;
        snd_pcm_t *handle;
        char *s = (char*)malloc(1024);

        fprintf(stderr, "Device for playback: %s\n", device);
        /*err = snd_output_stdio_attach(&output, stdout, 0);
        if (err < 0) {
                printf("Output failed: %s\n", snd_strerror(err));
                return 0;
        }*/
        if (err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0) < 0) {
                fprintf(stderr, "Error opening device for playback, exiting.\n");
                exit(1);
        }
        fprintf(stderr, "Opened device %s successfully.\n", device);

        fprintf(stderr, "PCM name: %s\n", snd_pcm_name(handle));

        snd_pcm_close(handle);
}

