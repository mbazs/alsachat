
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "replay.h"

int main(int argc, char **argv) {
	char c;
	char *device = NULL;
	enum {
		NONE,
		REPLAY
	} action;

	struct option long_option[] =
	{
		{"help", 0, NULL, 'h'},
		{"device", 1, NULL, 'D'},
		{"replay", 0, NULL, 'r'},

		{NULL, 0, NULL, 0},
	};

	device = (char*)malloc(200);

	while (1) {
		if ((c = getopt_long(argc, argv, "hD:r", long_option, NULL)) < 0) {
			break;
		}

		switch (c) {
			case 'h':
				printf("help");
				break;
			case 'D': 
				strcpy(device, optarg);
				break;
			case 'r':
				action = REPLAY;	
				replay(device);
				break;
			default:
				break;
		}
	}

	if (action == NONE) {
		fprintf(stderr, "There weren't any action(s).\n");
	}

	fprintf(stderr, "Completed.\n");
}


